import React from 'react';

import Navbar from '../components/Navbar/NavbarFeatures';
import Content from '../components/Content/Content';
import Footer from '../components/Footer/FooterPage';

class Home extends React.Component {
    render() {
        return (
            <div>
        <Navbar />
    <Content />
        <Footer />
      </div>
        ); 
    }  
}
export default  Home ;