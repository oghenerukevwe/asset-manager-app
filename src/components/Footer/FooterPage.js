import React from 'react';
import { Col, Container, Row, Footer } from 'mdbreact';

class FooterPage extends React.Component {
    render(){
        return(
            <Footer color="indigo">
            <p className="footer-copyright mb-0 py-3 text-center">
              &copy; {(new Date().getFullYear())} Copyright: <a href="#"> Booust Asset Manager </a>
            </p>
          </Footer>
        );
    }
}



export default FooterPage;