import React from "react";

import "./Content.css";
import enterprise from  "./enterprise.jpg";

import { Container, Col, Row, CardBody, Fa } from "mdbreact";

const NavLink = require("react-router-dom").NavLink;

var CardStyle = {
    backgroundImage: "url(" + enterprise + ")"
  };

class Content extends React.Component {
  render() {
    return (
      <div>
      <div className=" animated fadeIn mt-sm-5 mb-sm-5 pt-sm-4">
      <div className="card card-image" style={CardStyle}>
        <div className="mask flex-center rgba-black-strong">
          <div className=" container-fluid full-bg-img d-flex align-items-center justify-content-center">
            <div className="row d-flex justify-content-center animated   zoomIn ">
              <div className="col-md-10 text-center">
              <h2 className="display-4 font-weight-bold white-text pt-5 mb-2">booust Asset Manager</h2>
              <hr className="hr-light"></hr>
              <h4 className="white-text my-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti
                consequuntur.
                </h4>
              <button type="button" className="btn btn-outline-white">Learn  more<i className="fa fa-book ml-2"></i></button>
              </div>
            </div>
          </div>
        </div>
        </div>
      
    </div>
        <Container Classname="mt-5 pt-5">
          <Row>
            <Col md="10" className="mx-auto mt-5 pt-5">
              <Row id="categories">
              <Col md="4" className="mb-5">
              <Col size="2" md="2" className="float-left">
                <Fa icon="book" className="blue-text" size="2x" />
              </Col>
              <Col size="10" md="8" lg="10" className="float-right">
                <h4 className="font-weight-bold">ABOUT US</h4>
                <p className="black-text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                 
                </p>
                <NavLink
                  tag="button"
                  className="btn btn-sm indigo darken-3"
                  to="/components"
                >
                  Learn more
                </NavLink>
              </Col>
            </Col>
                <Col md="4" className="mb-5">
                  <Col size="2" md="2" className="float-left">
                    <Fa icon="cubes" className="blue-text" size="2x" />
                  </Col>
                  <Col size="10" md="8" lg="10" className="float-right">
                    <h4 className="font-weight-bold">COMPONENTS</h4>
                    <p className="black-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    </p>
                    <NavLink
                      tag="button"
                      className="btn btn-sm indigo darken-3"
                      to="/components"
                    >
                      Learn more
                    </NavLink>
                  </Col>
                </Col>
                <Col md="4" className="mb-5">
                  <Col size="2" md="2" className="float-left">
                    <Fa icon="code" className="green-text" size="2x" />
                  </Col>
                  <Col size="10" md="8" lg="10" className="float-right">
                    <h4 className="font-weight-bold">ADVANCED</h4>
                    <p className="black-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    </p>
                    <NavLink
                      tag="button"
                      className="btn btn-sm indigo darken-3"
                      to="/advanced"
                    >
                      Learn more
                    </NavLink>
                  </Col>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Content;
