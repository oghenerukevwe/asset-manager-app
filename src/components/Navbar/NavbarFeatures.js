import React, { Component } from "react";
import {
  Navbar,
  NavbarBrand,
  NavbarNav,
  NavbarToggler,
  Collapse,
  NavItem,
  NavLink,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "mdbreact";
import { BrowserRouter as Router } from "react-router-dom";

class NavbarFeatures extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapse: false,
      isWideEnough: false
    };
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.setState({
      collapse: !this.state.collapse
    });
  }
  render() {
    return (
      <Router>
        <Navbar color="indigo" dark expand="md" fixed="top" scrolling>
          <NavbarBrand className="font-bold white-text" href="/">
            <strong>Booust Asset Manager</strong>
          </NavbarBrand>
          {!this.state.isWideEnough && <NavbarToggler onClick={this.onClick} />}
          <Collapse isOpen={this.state.collapse} navbar>
            <NavbarNav right >
              <NavItem active>
                <NavLink  to="#">Home</NavLink>
              </NavItem>
              <NavItem>
                <NavLink to="#">Features</NavLink>
              </NavItem>
              <NavItem>
                <NavLink to="#">Login</NavLink>
              </NavItem>
            </NavbarNav>
            <NavbarNav right>
              <NavItem>
                <form className="form-inline  w-100 my-2 my-lg-0 ml-5 ">
                  <input
                    class="form-control mr-sm-2"
                    type="text"
                    placeholder="Search movies"
                  />
                  <button
                    class="btn btn-info btn-rounded btn-sm my-2 my-sm-0 waves-effect waves-light"
                    type="submit"
                  >
                    Search{" "}
                  </button>
                </form>
              </NavItem>
            </NavbarNav>
          </Collapse>
        </Navbar>
      </Router>
    );
  }
}
export default NavbarFeatures;
